class Box :
	def __init__(self):
		self._contents = []
		self._ouvert=True
		self._capacity=None
		
	def add(self, truc):
		self._contents.append(truc)
	
	def __contains__(self, machin):
		return machin in self._contents
	
	def remove(self, bidule):
		self._contents.remove(bidule)
		
	def is_ouvert(self):
		return self._ouvert
	
	def ouvert(self):
		self._ouvert=True
	
	def close(self):
		self._ouvert=False

	def action_look(self):
		res="La boite contient : "
		if not self.is_ouvert() :
			return "La boite est fermée"
		else :
			for elem in self._contents:
				res+=elem
				return res
	
	def set_capacity(self, cap):
		self._capacity=cap
		
	def capacity(self):
		return self._capacity
		
	def has_room_for(self, thing):
		if self.capacity()==None :
			return True
		else:
			return self.capacity()>thing.volume()
			
	def action_add(self, thing):
		if self.is_ouvert():
			if self.has_room_for(thing):
				self.add(thing)
				self._capacity-=thing.volume()
				return True
			else :
				return False
		else :
			return False
	
	def find(self, name):
		if self.__contains__(name):
			return name
		else :
			return None
	

class Thing :
	def __init__(self, volume):
		self._volume=volume
		self._name=""
		
	def volume(self):
		return self._volume
	
	def set_name(self, name):
		self._name=name
		
	def __repr__(self):
		return self.str()
	
	def has_name(self):
		return len(self._name)>0
			
