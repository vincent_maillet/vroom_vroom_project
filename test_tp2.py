from tp2 import *

def test_box_create():
	b = Box()
	b.add("truc1")
	b.add("truc2")
	assert "truc1" in b
	assert "truc3" not in b
	b.add("bidule")
	assert "bidule" in b
	b.remove("bidule")
	assert "bidule" not in b
	
def test_box_ouvert():
	b = Box()
	b.ouvert()
	assert b.is_ouvert()
	b.close()
	assert not b.is_ouvert()

def test_look():
	b = Box()
	b.add('chose')
	b.ouvert()
	assert b.action_look()=="La boite contient : chose"
	b.close()
	assert b.action_look()=="La boite est fermée"

def test_volume():
	t = Thing(3)
	assert t._volume==3
	assert t.volume()==3

def test_capacity():
	b = Box()
	t= Thing(3)
	assert b.capacity()==None
	assert b.has_room_for(t)
	b.set_capacity(5)
	assert b.capacity()==5
	assert b.has_room_for(t)
	assert b.action_add(t)
	assert b.capacity()==2
	b.close()
	assert not b.action_add(t)

def test_name():
	b=Box()
	t=Thing(3)
	b.set_capacity(5)
	assert not t.has_name()
	t.set_name("bouyah")
	assert t.has_name()
	b.action_add(t)
	assert b.find("bouyah")=="bouyah"
	assert b.find("hnsdjkfvbqsdmsùl")==None
	
